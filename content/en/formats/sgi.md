---
name: SGI
params:
- sgi
---
Output in the SGI image file format.
