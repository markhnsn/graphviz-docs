---
name: GTK canvas
params:
- gtk
---
Creates a [GTK](https://www.gtk.org/) window and displays the output there.
