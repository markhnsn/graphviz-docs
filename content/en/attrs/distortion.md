---
defaults:
- '0.0'
flags: []
minimums:
- '-100.0'
name: distortion
types:
- double
used_by: "N"
---
Distortion factor for [`shape`](#d:shape)`=polygon`.

Positive values cause top part to be larger than bottom; negative values do
the opposite.
