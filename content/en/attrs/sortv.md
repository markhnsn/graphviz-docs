---
defaults:
- '0'
flags: []
minimums:
- '0'
name: sortv
types:
- int
used_by: GCN
---
If [`packmode`](#d:packmode) indicates an array packing, `sortv` specifies an
insertion order among the components, with smaller values inserted first.
