---
defaults:
- '0.0'
flags: []
minimums:
- '0.0'
name: quantum
types:
- double
used_by: G
---
If `quantum > 0.0`, node label dimensions will be rounded to integral multiples of the quantum.
