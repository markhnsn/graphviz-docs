---
defaults:
- '1.0'
flags: []
minimums:
- '0.0'
name: labeldistance
types:
- double
used_by: E
---
Multiplicative scaling factor adjusting the distance that the
[`headlabel`](#d:headlabel) / [`taillabel`](#d:taillabel) is from the head /
tail node.

The default distance is 10 points.

See [`labelangle`](#d:labelangle) for more details.
